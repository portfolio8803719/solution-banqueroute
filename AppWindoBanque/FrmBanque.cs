﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using libBanque;

namespace AppWindoBanque
{
    public partial class FrmBanque : Form
    {
        // Variable globales au formulaire = Propriétés du formulaire
        private AgenceBancaire MonAgence;

        public FrmBanque()
        {
            InitializeComponent();
            InitialiserLaBanque();
        }

        private void InitialiserLaBanque()
        {
            MonAgence = new AgenceBancaire("MENTA");

            lbl_ac_Titre.Text = " AGENCE de " + MonAgence.NomAgence;

            GenererJeuDessai();

            RemplirListeComptes();
        }

        private void GenererJeuDessai()
        {
            CompteBancaire cbPatrick = new CompteBancaire("0211651079JP", "M.JANE Patrick");
            CompteBancaire cbTeresa = new CompteBancaire("1231212312LT", "Mle LISBON Teresa", 1500.00m);
            CompteBancaire cbCho = new CompteBancaire("9173826431KC", "M. KIMBALL Cho", 5050.00m);

            MonAgence.AjouterCompte(cbPatrick);
            MonAgence.AjouterCompte(cbTeresa);
            MonAgence.AjouterCompte(cbCho);

        }

        private void RemplirListeComptes()
        {
            dgv_ListeComptes.DataSource = MonAgence.LesComptes;

            dgv_ListeComptes.AutoResizeColumns();
        }
        


        private void FrmBanque_Load(object sender, EventArgs e)
        {

        }

        private void tabConsultation_Enter(object sender, EventArgs e)
        {
            RemplirCombobox(cbx_co_ChoixCompte);
        }

        /// <summary>
        /// Sous-programme permettant de remplir une combobox avec la liste des comptes
        /// </summary>
        /// <param name="laCombo">La combobox à remplir</param>
        /// <exception cref="NotImplementedException"></exception>
        private void RemplirCombobox(ComboBox laCombo)
        {
            // supprimer le contenu de la combobox
            laCombo.Items.Clear();
            // remplir la combox à l'aide de la structure itérative foreach
            foreach (CompteBancaire unCompte in MonAgence.LesComptes)
            {
                //...Ajouter le numero du compte suivi du titulaire dans les items de la combobox
                laCombo.Items.Add(unCompte.NumCompte + " (" + unCompte.NomTitulaire + ")");
            }
        }
    }
}
