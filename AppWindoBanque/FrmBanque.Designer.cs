﻿namespace AppWindoBanque
{
    partial class FrmBanque
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab_OngletsApplication = new System.Windows.Forms.TabControl();
            this.tabAcceuil = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_ac_Titre = new System.Windows.Forms.Label();
            this.dgv_ListeComptes = new System.Windows.Forms.DataGridView();
            this.tabConsultation = new System.Windows.Forms.TabPage();
            this.tabCreation = new System.Windows.Forms.TabPage();
            this.tabVersement = new System.Windows.Forms.TabPage();
            this.tabRetrait = new System.Windows.Forms.TabPage();
            this.tabVirement = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.cbx_co_ChoixCompte = new System.Windows.Forms.ComboBox();
            this.tbx_co_Numero = new System.Windows.Forms.TextBox();
            this.tbx_co_Titulaire = new System.Windows.Forms.TextBox();
            this.tbx_co_SoldeActuel = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tab_OngletsApplication.SuspendLayout();
            this.tabAcceuil.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListeComptes)).BeginInit();
            this.tabConsultation.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab_OngletsApplication
            // 
            this.tab_OngletsApplication.Controls.Add(this.tabAcceuil);
            this.tab_OngletsApplication.Controls.Add(this.tabConsultation);
            this.tab_OngletsApplication.Controls.Add(this.tabCreation);
            this.tab_OngletsApplication.Controls.Add(this.tabVersement);
            this.tab_OngletsApplication.Controls.Add(this.tabRetrait);
            this.tab_OngletsApplication.Controls.Add(this.tabVirement);
            this.tab_OngletsApplication.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tab_OngletsApplication.Location = new System.Drawing.Point(0, 0);
            this.tab_OngletsApplication.Name = "tab_OngletsApplication";
            this.tab_OngletsApplication.SelectedIndex = 0;
            this.tab_OngletsApplication.Size = new System.Drawing.Size(834, 482);
            this.tab_OngletsApplication.TabIndex = 0;
            // 
            // tabAcceuil
            // 
            this.tabAcceuil.Controls.Add(this.label1);
            this.tabAcceuil.Controls.Add(this.lbl_ac_Titre);
            this.tabAcceuil.Controls.Add(this.dgv_ListeComptes);
            this.tabAcceuil.Location = new System.Drawing.Point(4, 25);
            this.tabAcceuil.Name = "tabAcceuil";
            this.tabAcceuil.Padding = new System.Windows.Forms.Padding(3);
            this.tabAcceuil.Size = new System.Drawing.Size(792, 421);
            this.tabAcceuil.TabIndex = 0;
            this.tabAcceuil.Text = "Acceuil";
            this.tabAcceuil.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Crimson;
            this.label1.Location = new System.Drawing.Point(64, 131);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(237, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Liste des comptes de l\'agence";
            // 
            // lbl_ac_Titre
            // 
            this.lbl_ac_Titre.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ac_Titre.ForeColor = System.Drawing.Color.Crimson;
            this.lbl_ac_Titre.Location = new System.Drawing.Point(190, 37);
            this.lbl_ac_Titre.Name = "lbl_ac_Titre";
            this.lbl_ac_Titre.Size = new System.Drawing.Size(465, 85);
            this.lbl_ac_Titre.TabIndex = 1;
            this.lbl_ac_Titre.Text = "label1";
            // 
            // dgv_ListeComptes
            // 
            this.dgv_ListeComptes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ListeComptes.Location = new System.Drawing.Point(44, 184);
            this.dgv_ListeComptes.Name = "dgv_ListeComptes";
            this.dgv_ListeComptes.RowHeadersWidth = 51;
            this.dgv_ListeComptes.RowTemplate.Height = 24;
            this.dgv_ListeComptes.Size = new System.Drawing.Size(700, 203);
            this.dgv_ListeComptes.TabIndex = 0;
            // 
            // tabConsultation
            // 
            this.tabConsultation.Controls.Add(this.label6);
            this.tabConsultation.Controls.Add(this.label5);
            this.tabConsultation.Controls.Add(this.label4);
            this.tabConsultation.Controls.Add(this.label3);
            this.tabConsultation.Controls.Add(this.tbx_co_SoldeActuel);
            this.tabConsultation.Controls.Add(this.tbx_co_Titulaire);
            this.tabConsultation.Controls.Add(this.tbx_co_Numero);
            this.tabConsultation.Controls.Add(this.cbx_co_ChoixCompte);
            this.tabConsultation.Controls.Add(this.label2);
            this.tabConsultation.Location = new System.Drawing.Point(4, 25);
            this.tabConsultation.Name = "tabConsultation";
            this.tabConsultation.Padding = new System.Windows.Forms.Padding(3);
            this.tabConsultation.Size = new System.Drawing.Size(826, 453);
            this.tabConsultation.TabIndex = 1;
            this.tabConsultation.Text = "Consultation";
            this.tabConsultation.UseVisualStyleBackColor = true;
            this.tabConsultation.Enter += new System.EventHandler(this.tabConsultation_Enter);
            // 
            // tabCreation
            // 
            this.tabCreation.Location = new System.Drawing.Point(4, 25);
            this.tabCreation.Name = "tabCreation";
            this.tabCreation.Size = new System.Drawing.Size(792, 421);
            this.tabCreation.TabIndex = 2;
            this.tabCreation.Text = "Création";
            this.tabCreation.UseVisualStyleBackColor = true;
            // 
            // tabVersement
            // 
            this.tabVersement.Location = new System.Drawing.Point(4, 25);
            this.tabVersement.Name = "tabVersement";
            this.tabVersement.Size = new System.Drawing.Size(792, 421);
            this.tabVersement.TabIndex = 3;
            this.tabVersement.Text = "Versement";
            this.tabVersement.UseVisualStyleBackColor = true;
            // 
            // tabRetrait
            // 
            this.tabRetrait.Location = new System.Drawing.Point(4, 25);
            this.tabRetrait.Name = "tabRetrait";
            this.tabRetrait.Size = new System.Drawing.Size(792, 421);
            this.tabRetrait.TabIndex = 4;
            this.tabRetrait.Text = "Retrait";
            this.tabRetrait.UseVisualStyleBackColor = true;
            // 
            // tabVirement
            // 
            this.tabVirement.Location = new System.Drawing.Point(4, 25);
            this.tabVirement.Name = "tabVirement";
            this.tabVirement.Size = new System.Drawing.Size(792, 421);
            this.tabVirement.TabIndex = 5;
            this.tabVirement.Text = "Virement";
            this.tabVirement.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label2.Location = new System.Drawing.Point(150, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(485, 63);
            this.label2.TabIndex = 0;
            this.label2.Text = "CONSULTATION DE COMPTE";
            // 
            // cbx_co_ChoixCompte
            // 
            this.cbx_co_ChoixCompte.FormattingEnabled = true;
            this.cbx_co_ChoixCompte.Location = new System.Drawing.Point(287, 145);
            this.cbx_co_ChoixCompte.Name = "cbx_co_ChoixCompte";
            this.cbx_co_ChoixCompte.Size = new System.Drawing.Size(247, 24);
            this.cbx_co_ChoixCompte.TabIndex = 1;
            // 
            // tbx_co_Numero
            // 
            this.tbx_co_Numero.Location = new System.Drawing.Point(287, 249);
            this.tbx_co_Numero.Name = "tbx_co_Numero";
            this.tbx_co_Numero.Size = new System.Drawing.Size(158, 22);
            this.tbx_co_Numero.TabIndex = 2;
            // 
            // tbx_co_Titulaire
            // 
            this.tbx_co_Titulaire.Location = new System.Drawing.Point(287, 297);
            this.tbx_co_Titulaire.Name = "tbx_co_Titulaire";
            this.tbx_co_Titulaire.Size = new System.Drawing.Size(300, 22);
            this.tbx_co_Titulaire.TabIndex = 3;
            // 
            // tbx_co_SoldeActuel
            // 
            this.tbx_co_SoldeActuel.Location = new System.Drawing.Point(287, 346);
            this.tbx_co_SoldeActuel.Name = "tbx_co_SoldeActuel";
            this.tbx_co_SoldeActuel.Size = new System.Drawing.Size(158, 22);
            this.tbx_co_SoldeActuel.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label3.Location = new System.Drawing.Point(59, 152);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Choisir un compte";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label4.Location = new System.Drawing.Point(59, 251);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(151, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Numéro du compte";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label5.Location = new System.Drawing.Point(59, 299);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(152, 20);
            this.label5.TabIndex = 7;
            this.label5.Text = "Titulaire du compte";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label6.Location = new System.Drawing.Point(59, 348);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(184, 20);
            this.label6.TabIndex = 8;
            this.label6.Text = "Solde actuel du compte";
            // 
            // FrmBanque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 482);
            this.Controls.Add(this.tab_OngletsApplication);
            this.Name = "FrmBanque";
            this.Text = "Application Banqueroute";
            this.Load += new System.EventHandler(this.FrmBanque_Load);
            this.tab_OngletsApplication.ResumeLayout(false);
            this.tabAcceuil.ResumeLayout(false);
            this.tabAcceuil.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListeComptes)).EndInit();
            this.tabConsultation.ResumeLayout(false);
            this.tabConsultation.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tab_OngletsApplication;
        private System.Windows.Forms.TabPage tabAcceuil;
        private System.Windows.Forms.TabPage tabConsultation;
        private System.Windows.Forms.TabPage tabCreation;
        private System.Windows.Forms.TabPage tabVersement;
        private System.Windows.Forms.TabPage tabRetrait;
        private System.Windows.Forms.TabPage tabVirement;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_ac_Titre;
        private System.Windows.Forms.DataGridView dgv_ListeComptes;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbx_co_SoldeActuel;
        private System.Windows.Forms.TextBox tbx_co_Titulaire;
        private System.Windows.Forms.TextBox tbx_co_Numero;
        private System.Windows.Forms.ComboBox cbx_co_ChoixCompte;
        private System.Windows.Forms.Label label2;
    }
}

