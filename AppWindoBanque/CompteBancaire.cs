﻿namespace libBanque
{
    public class CompteBancaire
    {
        #region Propriétés privées de la classe

        // Propriétés privées
        private string numCompte;
        private string nomTitulaire;
        private decimal soldeCompte;

        #endregion

        #region Property = Accesseurs (publiques) aux propriétés

        // Property = Accesseur (publiques) aux propriétés
        public string NumCompte { get => numCompte; set => numCompte = value; }
        public string NomTitulaire { get => nomTitulaire; set => nomTitulaire = value; }
        public decimal SoldeCompte { get => soldeCompte; }

        #endregion

        #region Constructeurs de la classe
        /// <summary>
        /// Initialise une nouvelle Instance de la classe libBanque.CompteBancaire
        /// </summary>
        /// <param name="sonNumero">Le numéro du compte bancaire</param>
        /// <param name="sonTitulaire">Le nom du titulaire du compte : nom + prénom</param>
        /// <remarks>Le solde du compte sera initialisé à 0</remarks>
        public CompteBancaire(string sonNumero, string sonTitulaire)
        {
            numCompte = sonNumero;
            nomTitulaire = sonTitulaire;
            soldeCompte = 0;
        }

        public CompteBancaire(string sonNumero, string sonTitulaire, decimal sonSoldeInitial)
        {
            numCompte = sonNumero;
            nomTitulaire = sonTitulaire;
            soldeCompte = sonSoldeInitial;
        }

        #endregion

        
    }

}
