﻿namespace libBanque
{
    public class CompteBancaire
    {
        #region Propriétés privées de la classe

        // Propriétés privées
        private string numCompte;
        private string nomTitulaire;
        private Decimal soldeCompte;

        #endregion

        #region Property = Accesseurs (publiques) aux propriétés

        // Property = Accesseur (publiques) aux propriétés
        public string NumCompte { get => numCompte; set => numCompte = value; }
        public string NomTitulaire { get => nomTitulaire; set => nomTitulaire = value; }
        public decimal SoldeCompte { get => soldeCompte; }

        #endregion
    }
}